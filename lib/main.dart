import 'package:flutter/material.dart';

import 'locator.dart';
import 'navigator_service.dart';
import 'router.dart';

void main() {
  setupLocator();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final NavigatorService navigator = sl.get<NavigatorService>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Delivery App',
      theme: ThemeData(primaryColor: Colors.blue, ),
      initialRoute: Router.splashScreen,
      navigatorKey: navigator.navigatorKey,
      onGenerateRoute: Router().generateRoute,
    );
  }
}
