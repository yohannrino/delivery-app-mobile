import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'src/models/screen_argument.dart';
import 'src/screens/main_screen/main_screen.dart';
import 'src/screens/manage_drivers/manage_drivers_screen.dart';
import 'src/screens/map_screen/map_screen.dart';
import 'src/screens/order_screen/order_screen.dart';
import 'src/screens/place_order_screen/place_order_screen.dart';
import 'src/screens/sign_in_screen/sign_in_screen.dart';
import 'src/screens/splash_screen/splash_screen.dart';

class Router {
  static const String placeOrderScreen = 'PlaceOrderScreen';
  static const String splashScreen = 'SplashScreen';
  static const String signInScreen = 'SignInScreen';
  static const String mainScreen = 'MainScreen';
  static const String mapScreen = 'MapScreen';
  static const String orderScreen = 'OrderScreen';
  static const String manageDriversScreen = 'ManageDriversScreen';

  Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case placeOrderScreen:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => PlaceOrderScreen());
      case splashScreen:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => SplashScreen());
      case signInScreen:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => SignInScreen());
      case mainScreen:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => MainScreen());
      case mapScreen:
        final MapScreenArgument args = settings.arguments;

        return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => MapScreen(
            transaction: args.transaction,
            flag: args.flag,
          ),
        );
      case orderScreen:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => OrderScreen());
      case manageDriversScreen:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => ManageDriversScreen());
      default:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => MainScreen());
    }
  }
}
