import 'package:dio/dio.dart';

class DioClient {
  DioClient._();

  DioClient.createInstance({BaseOptions baseOptions}) {
    dio = Dio();
    dio.options = baseOptions;

    _baseOptions = baseOptions;
  }

  Dio dio;
  BaseOptions _baseOptions;

  void setBaseOptions([BaseOptions baseOptions]) {
    if (baseOptions == null) {
      dio.options = _baseOptions;
      return;
    }

    dio.options = baseOptions;
  }
}
