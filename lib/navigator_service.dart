import 'package:flutter/material.dart';

class NavigatorService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<dynamic> pushNamed(String routeName, {Object arguments}) {
    return navigatorKey.currentState.pushNamed<dynamic>(routeName, arguments: arguments);
  }

  Future<dynamic> pushNamedAndRemoveUntil(
    Route<dynamic> route,
    String predicate,
  ) {
    return navigatorKey.currentState.pushAndRemoveUntil(
      route,
      ModalRoute.withName(predicate),
    );
  }

  Future<dynamic> pushReplacement(Widget screen) {
    final Route<dynamic> route = MaterialPageRoute<dynamic>(
      builder: (BuildContext context) => screen,
    );
    return navigatorKey.currentState.pushReplacement(route);
  }

  bool pop([dynamic results]) {
    return navigatorKey.currentState.pop<dynamic>(results);
  }
}
