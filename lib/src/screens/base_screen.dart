import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

import '../../locator.dart';
import '../../navigator_service.dart';
import '../constants/Fonts.dart';
import '../managers/cancel_token_manager.dart';
import '../resources/color_res.dart';

abstract class BaseStatefulWidget extends StatefulWidget {}

abstract class BaseState<T extends StatefulWidget> extends State<T> {
  @protected
  final NavigatorService navigatorService = sl.get<NavigatorService>();
  @protected
  Logger logger = Logger();

  @override
  void dispose() {
    CancelTokenManager.cancel();
    super.dispose();
  }

  @protected
  void showAlertDialog({
    String title = '',
    String content = '',
    String positiveText,
    String negativeText,
    bool barrierDismissible = true,
    void Function() onPositiveActionPress,
    void Function() onNegativeActionPress,
  }) {
    showDialog<dynamic>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            title: Text(
              title,
              style: const TextStyle(fontFamily: Fonts.avenirLTStdHeavy),
            ),
            content: Text(
              content,
              style: TextStyle(fontFamily: Fonts.avenirLTStdMedium),
            ),
            actions: <Widget>[
              negativeText != null
                  ? FlatButton(
                      child: Text(
                        negativeText,
                        style: TextStyle(
                          color: ColorRes.primary,
                          fontFamily: Fonts.avenirLTStdHeavy,
                        ),
                      ),
                      onPressed:
                          onNegativeActionPress ?? () => Navigator.pop(context),
                    )
                  : Container(),
              positiveText != null
                  ? FlatButton(
                      child: Text(
                        positiveText,
                        style: TextStyle(
                          color: ColorRes.primary,
                          fontFamily: Fonts.avenirLTStdHeavy,
                        ),
                      ),
                      onPressed:
                          onPositiveActionPress ?? () => Navigator.pop(context),
                    )
                  : Container(),
            ],
          ),
        );
      },
    );
  }
}
