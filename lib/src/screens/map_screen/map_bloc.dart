import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deliveryapp/src/models/transaction.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../locator.dart';
import '../../models/geocode.dart';
import '../../services/map_service.dart';
import 'map_event.dart';
import 'map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
  final MapService _mapService = sl.get<MapService>();

  @override
  MapState get initialState => InitialMapState();

  @override
  Stream<MapState> mapEventToState(
    MapEvent event,
  ) async* {
    if (event is GetCurrentLocation) {
      yield SearchingLocation();
      yield* _getCurrentLocation(event);
    }

    if (event is SearchLocation) {
      yield SearchLocationPress();
    }

    if (event is GetSearchLocation) {
      yield* _getSearchLocation(event);
    }
  }

  Stream<MapState> _getCurrentLocation(GetCurrentLocation event) async* {
    final Position position = await Geolocator()
        .getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
    final Geocode geocode = await _mapService.getGeocode(
        latLng: LatLng(position.latitude, position.longitude));
    final BitmapDescriptor markerIcon =
        await createMarkerImageFromAsset('assets/images/ic_map_marker.png');

    final Transaction transaction = Transaction();

    if (geocode.results.isNotEmpty) {
      final Results result = geocode.results[0];
      final Location location = result.geometry.location;

      if (event.flag == 0) {
        transaction.pickUpLocation = PickUpLocation.fromJson(
          <String, dynamic>{
            'address': result.formattedAddress,
            'lat': location.lat,
            'lng': location.lng
          },
        );
      } else {
        transaction.dropOffLocation = DropOffLocation.fromJson(
          <String, dynamic>{
            'address': result.formattedAddress,
            'lat': location.lat,
            'lng': location.lng
          },
        );
      }
    }

    yield SearchAddressComplete(
      transaction: transaction,
      markerIcon: markerIcon,
    );
  }

  Stream<MapState> _getSearchLocation(GetSearchLocation event) async* {
    yield SearchingLocation();

    final Geocode geocode = await _mapService.getGeocode(address: event.query);
    final BitmapDescriptor markerIcon =
        await createMarkerImageFromAsset('assets/images/ic_map_marker.png');
    final Transaction transaction = Transaction();

    if (geocode.results.isNotEmpty) {
      final Results result = geocode.results[0];
      final Location location = result.geometry.location;

      if (event.flag == 0) {
        transaction.pickUpLocation = PickUpLocation.fromJson(
          <String, dynamic>{
            'address': result.formattedAddress,
            'lat': location.lat,
            'lng': location.lng
          },
        );
      } else {
        transaction.dropOffLocation = DropOffLocation.fromJson(
          <String, dynamic>{
            'address': result.formattedAddress,
            'lat': location.lat,
            'lng': location.lng
          },
        );
      }
    }

    yield SearchAddressComplete(
      transaction: transaction,
      markerIcon: markerIcon,
    );
  }

  Future<BitmapDescriptor> createMarkerImageFromAsset(String iconPath) async {
    const ImageConfiguration configuration = ImageConfiguration();
    final BitmapDescriptor bitmapImage =
        await BitmapDescriptor.fromAssetImage(configuration, iconPath);

    return bitmapImage;
  }
}
