import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MapEvent extends Equatable {
  MapEvent([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class GetCurrentLocation extends MapEvent {
  GetCurrentLocation({this.flag}) : super(<dynamic>[flag]);

  final int flag;
}

class SearchLocation extends MapEvent {}

class GetSearchLocation extends MapEvent {
  GetSearchLocation({this.query, this.flag}) : super(<dynamic>[query, flag]);

  final String query;
  final int flag;
}
