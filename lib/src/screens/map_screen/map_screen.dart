import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../constants/Fonts.dart';
import '../../models/transaction.dart';
import '../../resources/color_res.dart';
import '../../resources/string_res.dart';
import '../../widgets/app_bar_widget.dart';
import '../../widgets/text_form_field_widget.dart';
import '../base_screen.dart';

import 'map_bloc.dart';
import 'map_event.dart';
import 'map_state.dart';

class MapScreen extends BaseStatefulWidget {
  MapScreen({this.transaction, this.flag});

  final Transaction transaction;
  final int flag;

  @override
  State<StatefulWidget> createState() => _MapScreenState();
}

class _MapScreenState extends BaseState<MapScreen> {
  final double _kLat = 0.000636;
  final double _kLng = 0.000019;

  final Completer<GoogleMapController> _controller =
      Completer<GoogleMapController>();
  final CameraPosition _initialCameraPosition = CameraPosition(
    target: const LatLng(12.8797, 121.7740),
    zoom: 4,
  );

  final TextEditingController _searchController = TextEditingController();
  final TextEditingController _roomController = TextEditingController();
  final TextEditingController _floorController = TextEditingController();
  final TextEditingController _buildingController = TextEditingController();
  final TextEditingController _blockController = TextEditingController();
  final TextEditingController _contactNameController = TextEditingController();
  final TextEditingController _contactPhoneController = TextEditingController();

  final FocusNode _floorNode = FocusNode();
  final FocusNode _buildingNode = FocusNode();
  final FocusNode _blockNode = FocusNode();
  final FocusNode _contactNameNode = FocusNode();
  final FocusNode _contactPhoneNode = FocusNode();

  final MapBloc _mapBloc = MapBloc();

  Transaction _transaction = Transaction();
  int _flag;

  @override
  void initState() {
    final Transaction current = widget.transaction;

    _transaction = current;
    _flag = widget.flag;

    final bool canGetCurrentLocation = _flag == 0
        ? current.pickUpLocation?.address == null
        : current.dropOffLocation?.address == null;

    if (_flag == 0) {
      _roomController.text = current.pickUpLocation?.room ?? '';
      _floorController.text = current.pickUpLocation?.floor ?? '';
      _buildingController.text = current.pickUpLocation?.building ?? '';
      _blockController.text = current.pickUpLocation?.block ?? '';
      _contactNameController.text = current.pickUpLocation?.contactName ?? '';
      _contactPhoneController.text = current.pickUpLocation?.contactPhone ?? '';
    } else {
      _roomController.text = current.dropOffLocation?.room ?? '';
      _floorController.text = current.dropOffLocation?.floor ?? '';
      _buildingController.text = current.dropOffLocation?.building ?? '';
      _blockController.text = current.dropOffLocation?.block ?? '';
      _contactNameController.text = current.dropOffLocation?.contactName ?? '';
      _contactPhoneController.text =
          current.dropOffLocation?.contactPhone ?? '';
    }

    if (canGetCurrentLocation) {
      _mapBloc.dispatch(GetCurrentLocation(flag: _flag));
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _onBackButtonPress();
        return false;
      },
      child: BlocListener<MapBloc, MapState>(
        bloc: _mapBloc,
        listener: (BuildContext context, MapState state) {
          if (state is SearchAddressComplete) {
            if (state.transaction.pickUpLocation == null &&
                state.transaction.dropOffLocation == null) {
              showAlertDialog(
                title: StringRes.searchLocation,
                content: StringRes.errLocationNotFound,
                barrierDismissible: false,
                negativeText: StringRes.close,
                onNegativeActionPress: () {
                  Navigator.pop(context);
                  _mapBloc.dispatch(SearchLocation());
                },
              );
            }
          }
        },
        child: BlocBuilder<MapBloc, MapState>(
          bloc: _mapBloc,
          condition: (_, MapState currentState) {
            if (currentState is SearchAddressComplete) {
              return currentState.transaction.pickUpLocation != null ||
                  currentState.transaction.dropOffLocation != null;
            }
            return true;
          },
          builder: (BuildContext context, MapState state) {
            final bool isSearchingLocation = state is SearchingLocation;
            final bool isSearchPress = state is SearchLocationPress;
            final Set<Marker> markers = <Marker>{};

            if (state is SearchAddressComplete) {
              _transaction = state.transaction;

              if (_transaction?.pickUpLocation != null ||
                  _transaction?.dropOffLocation != null) {
                final double lat = _flag == 0
                    ? _transaction.pickUpLocation?.lat
                    : _transaction.dropOffLocation?.lat;
                final double lng = _flag == 0
                    ? _transaction.pickUpLocation?.lng
                    : _transaction.dropOffLocation?.lng;

                markers.add(
                  Marker(
                    icon: state.markerIcon,
                    markerId: MarkerId('0'),
                    position: LatLng(lat, lng),
                  ),
                );

                _updateMapCameraPosition(CameraPosition(
                  target: LatLng(lat - _kLat, lng + _kLng),
                  zoom: 18,
                ));
              }
            }

            if (state is InitialMapState) {
              if (_transaction?.pickUpLocation != null ||
                  _transaction?.dropOffLocation != null) {
                final double lat = _flag == 0
                    ? _transaction.pickUpLocation?.lat
                    : _transaction.dropOffLocation?.lat;
                final double lng = _flag == 0
                    ? _transaction.pickUpLocation?.lng
                    : _transaction.dropOffLocation?.lng;

                markers.add(
                  Marker(
                    markerId: MarkerId('0'),
                    position: LatLng(lat, lng),
                  ),
                );

                _updateMapCameraPosition(CameraPosition(
                  target: LatLng(lat - _kLat, lng + _kLng),
                  zoom: 18,
                ));
              }
            }

            return Scaffold(
              appBar: _buildAppBar(isSearchPress: isSearchPress),
              body: Stack(
                children: <Widget>[
                  GoogleMap(
                    initialCameraPosition: _initialCameraPosition,
                    onMapCreated: _onMapCreated,
                    markers: markers,
                  ),
                  _buildSearchInput(isSearchPress: isSearchPress),
                  _buildBottomWidget(
                    state: state,
                    isSearchingLocation: isSearchingLocation,
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _mapBloc.dispose();
    _searchController.dispose();
    _roomController.dispose();
    _floorController.dispose();
    _buildingController.dispose();
    _blockController.dispose();
    _contactNameController.dispose();
    _contactPhoneController.dispose();
    _floorNode.dispose();
    _buildingNode.dispose();
    _blockNode.dispose();
    _contactNameNode.dispose();
    _contactPhoneNode.dispose();
    super.dispose();
  }

  Widget _buildAppBar({bool isSearchPress}) {
    if (isSearchPress) {
      return PreferredSize(
        child: Container(
          color: Colors.blue,
        ),
        preferredSize: const Size(0, 0),
      );
    }

    final String _title =
        _flag == 0 ? StringRes.pickUpLocation : StringRes.dropOffLocation;

    return AppBarWidget(
      containerHeight: 144,
      title: _title,
      hasBorder: false,
      leading: IconButton(
        color: Colors.white,
        icon: Icon(Feather.arrow_left),
        onPressed: _onBackButtonPress,
      ),
      actions: <Widget>[
        IconButton(
          color: Colors.white,
          icon: Icon(Feather.search),
          onPressed: _onSearchLocationPress,
        ),
        IconButton(
          color: Colors.white,
          icon: Icon(Feather.check),
          onPressed: _onSavePress,
        ),
      ],
    );
  }

  Widget _buildSearchInput({bool isSearchPress}) {
    if (isSearchPress) {
      final MediaQueryData mediaQuery = MediaQuery.of(context);

      return Align(
        alignment: Alignment.topCenter,
        child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.grey,
                blurRadius: 10,
              ),
            ],
            color: Colors.white,
          ),
          height: 56,
          margin: EdgeInsets.symmetric(
            vertical: mediaQuery.padding.top + 8,
            horizontal: 32,
          ),
          padding: const EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 16,
          ),
          child: TextFormFieldWidget(
            contentPadding: const EdgeInsets.symmetric(vertical: 8),
            controller: _searchController,
            dark: false,
            hasBorder: false,
            hintText: StringRes.searchLocation,
            suffixIcon: IconButton(
              icon: Icon(
                Feather.search,
                color: Colors.black,
              ),
              onPressed: _onSearchPress,
            ),
            onFieldSubmitted: (String _) => _onSearchPress(),
          ),
        ),
      );
    }

    return Container();
  }

  Widget _buildBottomWidget({
    MapState state,
    bool isSearchingLocation,
  }) {
    if (isSearchingLocation) {
      return Container(
        padding: const EdgeInsets.symmetric(vertical: 32),
        alignment: Alignment.bottomCenter,
        child: const SizedBox(
          height: 24,
          width: 24,
          child: CircularProgressIndicator(),
        ),
      );
    }

    if (state is SearchLocationPress) {
      return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          padding: const EdgeInsets.all(32),
          child: SizedBox(
            height: 40,
            width: double.infinity,
            child: RaisedButton(
              color: ColorRes.primary,
              elevation: 8,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              onPressed: _onSearchPress,
              child: Text(
                StringRes.search,
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: Fonts.avenirLTStdMedium,
                ),
              ),
            ),
          ),
        ),
      );
    }

    final String address = _flag == 0
        ? _transaction.pickUpLocation?.address ?? ''
        : _transaction.dropOffLocation?.address ?? '';

    return Align(
      alignment: Alignment.bottomCenter,
      child: SingleChildScrollView(
        child: Container(
          height: 280,
          margin: const EdgeInsets.all(24),
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(12)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                blurRadius: 20,
                color: Colors.grey,
                offset: const Offset(0, 8),
              )
            ],
            color: Colors.white,
          ),
          child: Column(
            children: <Widget>[
              Text(
                address,
                style: TextStyle(
                  fontFamily: Fonts.avenirLTStdHeavy,
                  fontSize: 18,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(top: 8),
                child: Text(
                  '${StringRes.info} (${StringRes.optional})',
                  style: TextStyle(
                    color: ColorRes.primary,
                    fontFamily: Fonts.avenirLTStdMedium,
                    fontSize: 14,
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  _buildInfoInputField(
                    textEditingController: _roomController,
                    nextFocusNode: _floorNode,
                    title: '${StringRes.room} : ',
                  ),
                  _buildInfoInputField(
                    textEditingController: _floorController,
                    focusNode: _floorNode,
                    nextFocusNode: _buildingNode,
                    title: '${StringRes.floor} : ',
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  _buildInfoInputField(
                    textEditingController: _buildingController,
                    focusNode: _buildingNode,
                    nextFocusNode: _blockNode,
                    title: '${StringRes.building} : ',
                    width: 64,
                  ),
                  _buildInfoInputField(
                    textEditingController: _blockController,
                    focusNode: _blockNode,
                    nextFocusNode: _contactNameNode,
                    title: '${StringRes.block} : ',
                  ),
                ],
              ),
              _buildInfoInputField(
                textEditingController: _contactNameController,
                focusNode: _contactNameNode,
                nextFocusNode: _contactPhoneNode,
                title: '${StringRes.contactName} : ',
                width: 112,
              ),
              _buildInfoInputField(
                textEditingController: _contactPhoneController,
                focusNode: _contactPhoneNode,
                title: '${StringRes.contactPhone} : ',
                width: 112,
                inputActionDone: true,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildInfoInputField({
    @required TextEditingController textEditingController,
    FocusNode focusNode,
    FocusNode nextFocusNode,
    String title,
    double width = 40,
    bool inputActionDone = false,
  }) {
    return Expanded(
      child: TextFormFieldWidget(
        prefixIcon: Container(
          width: width,
          alignment: Alignment.center,
          child: Text(
            title,
            style: TextStyle(fontFamily: Fonts.avenirLTStdHeavy),
          ),
        ),
        textStyle: TextStyle(
          fontFamily: Fonts.avenirLTStdMedium,
          fontSize: 14,
        ),
        controller: textEditingController,
        contentPadding: const EdgeInsets.symmetric(vertical: 15),
        focusNode: focusNode,
        textInputAction:
            inputActionDone ? TextInputAction.done : TextInputAction.next,
        onFieldSubmitted: (String _) => _onFieldSubmitted(nextFocusNode),
      ),
    );
  }

  Future<void> _updateMapCameraPosition(CameraPosition newPosition) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(newPosition));
  }

  void _onSavePress() {
    if (_flag == 0) {
      _transaction.pickUpLocation
        ..room = _roomController.text
        ..floor = _floorController.text
        ..building = _buildingController.text
        ..block = _blockController.text
        ..contactName = _contactNameController.text
        ..contactPhone = _contactPhoneController.text;
    } else {
      _transaction.dropOffLocation
        ..room = _roomController.text
        ..floor = _floorController.text
        ..building = _buildingController.text
        ..block = _blockController.text
        ..contactName = _contactNameController.text
        ..contactPhone = _contactPhoneController.text;
    }

    navigatorService.pop(_transaction);
  }

  void _onMapCreated(GoogleMapController controller) {
    if (!_controller.isCompleted) {
      _controller.complete(controller);
    }
  }

  void _onSearchLocationPress() {
    _mapBloc.dispatch(SearchLocation());
  }

  void _onSearchPress() {
    if (_searchController.text.trim().isNotEmpty) {
      _mapBloc.dispatch(GetSearchLocation(
        query: _searchController.text,
        flag: _flag,
      ));
    }
  }

  void _onFieldSubmitted(FocusNode focusNode) {
    focusNode?.requestFocus();
  }

  void _onBackButtonPress() {
    navigatorService.pop();
  }
}
