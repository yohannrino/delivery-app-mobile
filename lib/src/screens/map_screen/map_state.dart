import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';

import '../../models/transaction.dart';

@immutable
abstract class MapState extends Equatable {
  MapState([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class InitialMapState extends MapState {}

class SearchingLocation extends MapState {}

class SearchLocationPress extends MapState {}

class SearchAddressComplete extends MapState {
  SearchAddressComplete({this.transaction, this.markerIcon})
      : super(<dynamic>[transaction, markerIcon]);

  final Transaction transaction;
  final BitmapDescriptor markerIcon;
}
