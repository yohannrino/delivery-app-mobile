import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SignInEvent extends Equatable {
  SignInEvent([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class ShowPassword extends SignInEvent {
  ShowPassword({this.showPassword}) : super(<dynamic>[showPassword]);

  final bool showPassword;
}
