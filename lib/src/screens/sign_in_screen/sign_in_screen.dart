import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../locator.dart';
import '../../../navigator_service.dart';
import '../../constants/Fonts.dart';
import '../../resources/string_res.dart';
import '../../utils/validator_util.dart';
import '../../widgets/text_form_field_widget.dart';
import '../base_screen.dart';
import '../main_screen/main_screen.dart';

import 'sign_in_bloc.dart';
import 'sign_in_event.dart';
import 'sign_in_state.dart';

class SignInScreen extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _SignInScreenState();
}

class _SignInScreenState extends BaseState<SignInScreen> {
  final FocusNode _passwordNode = FocusNode();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final SignInBloc _signInBloc = SignInBloc();
  final NavigatorService _navigatorService = sl.get<NavigatorService>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(40.0),
          child: BlocBuilder<SignInBloc, SignInState>(
            bloc: _signInBloc,
            builder: (BuildContext context, SignInState state) {
              final bool showPassword =
                  state is SignInShowPassword && state.showPassword;

              return Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: kToolbarHeight),
                      child: Text(
                        StringRes.appName,
                        style: const TextStyle(
                          fontFamily: Fonts.avenirLTStdHeavy,
                          fontSize: 32,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    _buildEmailTextInput(),
                    _buildPasswordTextInput(showPassword),
                    _buildSignInButton(),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordNode.dispose();
    _passwordController.dispose();
    _signInBloc.dispose();
    super.dispose();
  }

  Container _buildEmailTextInput() {
    return Container(
      margin: const EdgeInsets.only(top: 32),
      child: TextFormFieldWidget(
        dark: true,
        controller: _emailController,
        label: StringRes.emailAddress,
        textInputType: TextInputType.emailAddress,
        textInputAction: TextInputAction.next,
        validator: (String value) {
          return ValidatorUtil.validate(
            value,
            <Rule>[Rule.required, Rule.email],
          );
        },
        onFieldSubmitted: (String value) {
          _passwordNode.nextFocus();
        },
      ),
    );
  }

  Container _buildPasswordTextInput(bool showPassword) {
    return Container(
      margin: const EdgeInsets.only(top: 16.0),
      child: TextFormFieldWidget(
        dark: true,
        controller: _passwordController,
        focusNode: _passwordNode,
        label: StringRes.password,
        obscureText: !showPassword,
        validator: (String value) {
          return ValidatorUtil.validate(value, <Rule>[Rule.required]);
        },
        suffixIcon: IconButton(
          icon: Icon(
            showPassword ? Icons.visibility : Icons.visibility_off,
            color: Colors.white70,
          ),
          onPressed: () => _onPasswordIconPress(),
        ),
      ),
    );
  }

  Container _buildSignInButton() {
    return Container(
      margin: const EdgeInsets.only(top: 56),
      child: SizedBox(
        width: double.infinity,
        height: 40,
        child: FlatButton(
          color: Colors.lightBlueAccent.withOpacity(0.4),
          onPressed: () => _onSignInPress(),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24.0),
          ),
          child: Text(
            StringRes.signIn.toUpperCase(),
            style: TextStyle(
              color: Colors.white70,
              fontFamily: Fonts.avenirLTStdHeavy,
            ),
          ),
        ),
      ),
    );
  }

  void _onPasswordIconPress() {
    _signInBloc.dispatch(ShowPassword());
  }

  void _onSignInPress() {
    if (!_formKey.currentState.validate()) {
      return;
    }

    _navigatorService.pushReplacement(MainScreen());
  }
}
