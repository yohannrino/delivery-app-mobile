import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SignInState extends Equatable {
  SignInState([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class InitialSignInState extends SignInState {}

class SignInShowPassword extends SignInState {
  SignInShowPassword({this.showPassword}) : super(<dynamic>[showPassword]);

  final bool showPassword;
}