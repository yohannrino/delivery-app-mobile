import 'dart:async';
import 'package:bloc/bloc.dart';

import 'sign_in_event.dart';
import 'sign_in_state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  @override
  SignInState get initialState => InitialSignInState();

  @override
  Stream<SignInState> mapEventToState(
    SignInEvent event,
  ) async* {
    final SignInState prevState = currentState;

    if (event is ShowPassword) {
      bool showPassword = false;

      if (prevState is SignInShowPassword) {
        showPassword = prevState.showPassword;
      }

      yield SignInShowPassword(showPassword: !showPassword);
    }
  }
}
