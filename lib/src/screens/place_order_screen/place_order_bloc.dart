import 'dart:async';
import 'package:bloc/bloc.dart';

import 'place_order_event.dart';
import 'place_order_state.dart';

class PlaceOrderBloc extends Bloc<PlaceOrderEvent, PlaceOrderState> {
  @override
  PlaceOrderState get initialState => InitialPlaceOrderState();

  @override
  Stream<PlaceOrderState> mapEventToState(
    PlaceOrderEvent event,
  ) async* {
    if (event is UpdateTransaction) {
      yield TransactionUpdated(transaction: event.transaction);
    }
  }
}
