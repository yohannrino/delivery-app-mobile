import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../models/transaction.dart';

@immutable
abstract class PlaceOrderEvent extends Equatable {
  PlaceOrderEvent([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class UpdateTransaction extends PlaceOrderEvent {
  UpdateTransaction({this.transaction}) : super(<dynamic>[transaction]);

  final Transaction transaction;
}
