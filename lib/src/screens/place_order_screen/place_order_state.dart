import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../models/transaction.dart';

@immutable
abstract class PlaceOrderState extends Equatable {
  PlaceOrderState([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class InitialPlaceOrderState extends PlaceOrderState {}

class TransactionUpdated extends PlaceOrderState {
  TransactionUpdated({this.transaction}) : super(<dynamic>[transaction]);
  
  final Transaction transaction;
}
