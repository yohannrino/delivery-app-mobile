import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

import '../../../router.dart';
import '../../constants/Fonts.dart';
import '../../models/screen_argument.dart';
import '../../models/transaction.dart';
import '../../resources/color_res.dart';
import '../../resources/string_res.dart';
import '../../widgets/dropdown_field_widget/dropdown_field_widget.dart';
import '../../widgets/text_form_field_widget.dart';
import '../base_screen.dart';

import 'place_order_bloc.dart';
import 'place_order_event.dart';
import 'place_order_state.dart';

class PlaceOrderScreen extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _PlaceOrderState();
}

class _PlaceOrderState extends BaseState<PlaceOrderScreen> {
  final List<String> _vehicles = <String>['Motorcycle', 'MPV', 'Truck'];
  final Transaction _transaction = Transaction();
  final PlaceOrderBloc _placeOrderBloc = PlaceOrderBloc();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PlaceOrderBloc, PlaceOrderState>(
      bloc: _placeOrderBloc,
      builder: (BuildContext context, PlaceOrderState state) {
        PickUpLocation pickUpLocation = PickUpLocation();
        DropOffLocation dropOffLocation = DropOffLocation();

        if (state is TransactionUpdated) {
          pickUpLocation = state.transaction.pickUpLocation;
          dropOffLocation = state.transaction.dropOffLocation;
        }

        return SingleChildScrollView(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: <Widget>[
              _buildContainer(
                child: Column(
                  children: <Widget>[
                    DropdownFieldWidget(
                      items: _vehicles,
                      onChange: (String value) {},
                    ),
                    Container(margin: const EdgeInsets.only(top: 8)),
                    _buildRow(
                        icon: FontAwesome.circle,
                        iconColor: Colors.blue,
                        text: StringRes.pickUpLocation,
                        flag: 0,
                        address: pickUpLocation),
                    _buildRow(
                        icon: Feather.map_pin,
                        iconColor: Colors.green,
                        text: StringRes.dropOffLocation,
                        flag: 1,
                        address: dropOffLocation),
                  ],
                ),
              ),
              _buildContainer(
                child: const TextFormFieldWidget(
                  label: StringRes.remarks,
                  maxLines: 3,
                  minLines: 1,
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                margin: const EdgeInsets.only(top: 16),
                child: SizedBox(
                  width: 112,
                  height: 40,
                  child: RaisedButton(
                    color: ColorRes.primary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    onPressed: () {},
                    child: Text(
                      StringRes.deliver,
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: Fonts.avenirLTStdHeavy,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _placeOrderBloc.dispose();
    super.dispose();
  }

  Widget _buildContainer({Widget child}) {
    return Container(
      margin: const EdgeInsets.only(top: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.all(Radius.circular(8.0)),
      ),
      padding: const EdgeInsets.all(24),
      child: child,
    );
  }

  Widget _buildRow({
    String text,
    IconData icon,
    Color iconColor,
    int flag,
    Address address,
  }) {
    final String info =
        "${address?.block != null ? 'BLK' : ''} ${address?.block ?? ''} ${address?.building ?? ''} ${address?.floor != null ? 'FLR' : ''} ${address?.floor ?? ''} ${address?.room != null ? 'RM' : ''} ${address?.room ?? ''}";
    final String contactName = address?.contactName ?? '';
    final String contactPhone = address?.contactPhone ?? '';

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 16),
      child: GestureDetector(
        onTap: () => _onRowPress(flag),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              icon,
              color: iconColor,
              size: 20,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(left: 16.0),
                    child: Text(
                      address?.address ?? text,
                      style: TextStyle(
                        fontFamily: Fonts.avenirLTStdMedium,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  info.trim().isNotEmpty
                      ? Container(
                          margin: const EdgeInsets.only(left: 16.0, top: 8),
                          child: Text(
                            info,
                            style: TextStyle(
                              color: Colors.grey,
                              fontFamily: Fonts.avenirLTStdBook,
                              fontSize: 14,
                            ),
                          ),
                        )
                      : Container(),
                  contactName.trim().isNotEmpty
                      ? Container(
                          margin: const EdgeInsets.only(left: 16.0, top: 4),
                          child: Text(
                            contactName,
                            style: TextStyle(
                              color: Colors.grey,
                              fontFamily: Fonts.avenirLTStdBook,
                              fontSize: 14,
                            ),
                          ),
                        )
                      : Container(),
                  contactPhone.trim().isNotEmpty
                      ? Container(
                          margin: const EdgeInsets.only(left: 16.0, top: 2),
                          child: Text(
                            contactPhone,
                            style: TextStyle(
                              color: Colors.grey,
                              fontFamily: Fonts.avenirLTStdBook,
                              fontSize: 14,
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onRowPress(int flag) {
    navigatorService
        .pushNamed(Router.mapScreen,
            arguments: MapScreenArgument(transaction: _transaction, flag: flag))
        .then(
      (dynamic results) {
        if (results != null) {
          if (flag == 0) {
            _transaction.pickUpLocation = results.pickUpLocation;
          } else {
            _transaction.dropOffLocation = results.dropOffLocation;
          }

          _placeOrderBloc.dispatch(
            UpdateTransaction(transaction: _transaction),
          );
        }
      },
    );
  }
}
