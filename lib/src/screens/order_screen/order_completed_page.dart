import 'package:flutter/material.dart';

import '../../resources/color_res.dart';
import '../../widgets/transaction_card_widget.dart';

import '../base_screen.dart';

class OrderCompletedPage extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _OrderCompletedPageState();
}

class _OrderCompletedPageState extends BaseState<OrderCompletedPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorRes.backgroundColor,
      child: ListView.builder(
        padding: const EdgeInsets.all(22),
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          return TransactionCardWidget(
            borderColor: Colors.green,
          );
        },
      ),
    );
  }
}
