import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../widgets/bottom_navigation_widget/bottom_navigation_widget_bloc.dart';
import '../../widgets/bottom_navigation_widget/bottom_navigation_widget_state.dart';
import '../base_screen.dart';

import 'order_cancelled_page.dart';
import 'order_completed_page.dart';
import 'order_on_going_page.dart';

class OrderScreen extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _OrderScreenState();
}

class _OrderScreenState extends BaseState<OrderScreen> {
  final PageController _pageController = PageController();

  bool isBottomNavTap = false;

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: <BlocListener<Bloc<dynamic, dynamic>, dynamic>>[
        BlocListener<BottomNavigationWidgetBloc, BottomNavigationWidgetState>(
          listener: (BuildContext context, BottomNavigationWidgetState state) {
            if (state is IndexUpdated) {
              _pageController.animateToPage(
                state.selectedIndex,
                duration: const Duration(milliseconds: 400),
                curve: Curves.easeInOut,
              );
            }
          },
        ),
      ],
      child: Container(
        child: PageView(
          controller: _pageController,
          physics: const NeverScrollableScrollPhysics(),
          children: <Widget>[
            OrderOnGoingPage(),
            OrderCompletedPage(),
            OrderCancelledPage(),
          ],
        ),
      ),
    );
  }
}
