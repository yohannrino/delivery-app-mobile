import 'package:flutter/material.dart';

import '../../resources/color_res.dart';
import '../../widgets/transaction_card_widget.dart';

import '../base_screen.dart';

class OrderCancelledPage extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _OrderCancelledPageState();
}

class _OrderCancelledPageState extends BaseState<OrderCancelledPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorRes.backgroundColor,
      child: ListView.builder(
        padding: const EdgeInsets.all(22),
        itemCount: 5,
        itemBuilder: (BuildContext context, int index) {
          return TransactionCardWidget(
            borderColor: Colors.red,
          );
        },
      ),
    );
  }
}
