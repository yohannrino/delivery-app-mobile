import 'package:flutter/material.dart';

import '../../resources/color_res.dart';
import '../../widgets/transaction_card_widget.dart';

import '../base_screen.dart';

class OrderOnGoingPage extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _OrderOnGoingPageState();
}

class _OrderOnGoingPageState extends BaseState<OrderOnGoingPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorRes.backgroundColor,
      child: ListView.builder(
        padding: const EdgeInsets.all(22),
        itemCount: 5,
        itemBuilder: (BuildContext context, int index) {
          return TransactionCardWidget(
            borderColor: ColorRes.primary,
          );
        },
      )
    );
  }
}
