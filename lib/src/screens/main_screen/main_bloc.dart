import 'dart:async';
import 'package:bloc/bloc.dart';

import 'main_event.dart';
import 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  @override
  MainState get initialState => InitialMainState();

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {
    if (event is OpenDrawer) {
      yield MainDrawerOpen();
    }
    if (event is CloseDrawer) {
      yield MainDrawerClose();
    }
  }
}
