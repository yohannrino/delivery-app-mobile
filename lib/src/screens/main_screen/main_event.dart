import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MainEvent extends Equatable {
  MainEvent([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class OpenDrawer extends MainEvent {}

class CloseDrawer extends MainEvent {}
