import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:hidden_drawer_menu/hidden_drawer/hidden_drawer_menu.dart';

import '../../models/bottom_navigation_item.dart';
import '../../resources/color_res.dart';
import '../../resources/string_res.dart';
import '../../widgets//app_bar_widget.dart';
import '../../widgets/bottom_navigation_widget/bottom_navigation_widget.dart';
import '../../widgets/bottom_navigation_widget/bottom_navigation_widget_bloc.dart';
import '../../widgets/main_drawer_menu.dart';

import '../base_screen.dart';
import '../manage_drivers/manage_drivers_screen.dart';
import '../order_screen/order_screen.dart';
import '../place_order_screen/place_order_screen.dart';

import 'main_bloc.dart';
import 'main_event.dart';
import 'main_state.dart';

class MainScreen extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainScreenState();
}

class _MainScreenState extends BaseState<MainScreen> {
  final MainBloc _mainBloc = MainBloc();

  bool _isDrawerListenerInit = false;
  bool _isDrawerOpen = false;
  SimpleHiddenDrawerBloc _drawerController;

  BottomNavigationWidgetBloc _orderBottomNavigationBloc;

  @override
  Widget build(BuildContext context) {
    return SimpleHiddenDrawer(
      menu: MainDrawerMenu(),
      screenSelectedBuilder: (int position, SimpleHiddenDrawerBloc controller) {
        final List<BlocProvider<Bloc<Object, Object>>> providers =
            <BlocProvider<Bloc<Object, Object>>>[];

        String appBarTitle;
        Widget currentScreen;
        Widget currentBottomNavigation;

        _drawerController ??= controller;
        _initDrawerListener(context);

        switch (position) {
          case 0:
            currentScreen = PlaceOrderScreen();
            appBarTitle = StringRes.placeOrder;
            break;
          case 1:
            appBarTitle = StringRes.orders;

            currentScreen = OrderScreen();
            currentBottomNavigation = BottomNavigationWidget(
              menuList: <BottomNavigationItem>[
                BottomNavigationItem(
                  activeColor: ColorRes.primary,
                  iconData: Feather.truck,
                  title: StringRes.onGoing,
                ),
                BottomNavigationItem(
                  activeColor: Colors.green,
                  iconData: Feather.package,
                  title: StringRes.completed,
                ),
                BottomNavigationItem(
                  activeColor: Colors.red,
                  iconData: Feather.x_circle,
                  title: StringRes.cancelled,
                )
              ],
            );

            _orderBottomNavigationBloc = BottomNavigationWidgetBloc();

            providers.add(BlocProvider<BottomNavigationWidgetBloc>(
              builder: (BuildContext context) => _orderBottomNavigationBloc,
            ));
            break;
          case 2:
            appBarTitle = StringRes.manageDrivers;

            currentScreen = ManageDriversScreen();
            currentBottomNavigation = BottomNavigationWidget(
              menuList: <BottomNavigationItem>[
                BottomNavigationItem(
                  activeColor: Colors.green,
                  iconData: Feather.user_check,
                  title: StringRes.favorite,
                ),
                BottomNavigationItem(
                  activeColor: Colors.red,
                  iconData: Feather.user_x,
                  title: StringRes.banned,
                ),
              ],
            );

            _orderBottomNavigationBloc = BottomNavigationWidgetBloc();

            providers.add(BlocProvider<BottomNavigationWidgetBloc>(
              builder: (BuildContext context) => _orderBottomNavigationBloc,
            ));
            break;
        }

        return MultiBlocProvider(
          providers: providers,
          child: BlocBuilder<MainBloc, MainState>(
            bloc: _mainBloc,
            builder: (BuildContext context, MainState state) {
              final bool isDrawerOpen = state is MainDrawerOpen;

              return Scaffold(
                appBar: AppBarWidget(
                  title: appBarTitle,
                  leading: IconButton(
                    icon: Icon(
                      isDrawerOpen ? Feather.arrow_left : Feather.menu,
                    ),
                    color: Colors.white,
                    onPressed: _onMenuPressed,
                  ),
                ),
                body: currentScreen,
                bottomNavigationBar: currentBottomNavigation,
              );
            },
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _drawerController.dispose();

    _mainBloc.dispose();
    _orderBottomNavigationBloc?.dispose();
    super.dispose();
  }

  void _initDrawerListener(BuildContext context) {
    if (!_isDrawerListenerInit) {
      _isDrawerListenerInit = true;
      _drawerController.getMenuStateListener().listen((MenuState state) {
        if (state == MenuState.open) {
          _isDrawerOpen = true;
        } else if (state == MenuState.closed) {
          _isDrawerOpen = false;
        }

        _updateMenuIcon();
      });
    }
  }

  void _updateMenuIcon() {
    if (_isDrawerOpen) {
      _mainBloc.dispatch(OpenDrawer());
    } else {
      _mainBloc.dispatch(CloseDrawer());
    }
  }

  void _onMenuPressed() {
    _drawerController.toggle();
    _updateMenuIcon();
  }
}
