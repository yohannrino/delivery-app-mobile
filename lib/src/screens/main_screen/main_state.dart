import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MainState extends Equatable {
  MainState([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class InitialMainState extends MainState {}

class MainDrawerOpen extends MainState {}

class MainDrawerClose extends MainState {}
