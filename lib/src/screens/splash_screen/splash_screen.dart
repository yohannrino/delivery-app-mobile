import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../base_screen.dart';
import '../sign_in_screen/sign_in_screen.dart';

class SplashScreen extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends BaseState<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future<dynamic>.delayed(
      const Duration(seconds: 5),
      () => navigatorService.pushReplacement(SignInScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      child: Center(
        child: const SizedBox(
          height: 32,
          width: 32,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          ),
        ),
      ),
    );
  }
}
