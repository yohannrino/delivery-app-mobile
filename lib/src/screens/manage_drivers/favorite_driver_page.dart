import 'package:flutter/material.dart';

import '../../resources/color_res.dart';
import '../../widgets/driver_card_widget.dart';

import '../base_screen.dart';

class FavoriteDriverPage extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _FavoriteDriverPageState();
}

class _FavoriteDriverPageState extends BaseState<FavoriteDriverPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorRes.backgroundColor,
      child: ListView.builder(
        padding: const EdgeInsets.all(16),
        itemCount: 19,
        itemBuilder: (BuildContext context, int index) {
          return const DriverCardWidget(
            url:
                'https://cdn.vox-cdn.com/thumbor/Dc8bBshDmxtKUCeTFovjt_pz_bM=/0x0:1777x999/1200x800/filters:focal(708x235:992x519)/cdn.vox-cdn.com/uploads/chorus_image/image/63756879/parabellumcover.0.jpg',
            title: 'John Wick',
            subtitle: 'johnwick@gmail.com',
          );
        },
      ),
    );
  }
}
