import 'package:flutter/material.dart';

import '../../resources/color_res.dart';
import '../../widgets/driver_card_widget.dart';

import '../base_screen.dart';

class BannedDriverPage extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _BannedDriverPageState();
}

class _BannedDriverPageState extends BaseState<BannedDriverPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorRes.backgroundColor,
      child: ListView.builder(
        padding: const EdgeInsets.all(16),
        itemCount: 19,
        itemBuilder: (BuildContext context, int index) {
          return const DriverCardWidget(
            url:
                'https://image.tmdb.org/t/p/w500_and_h282_face/5elIQbN8uuXSjpIg4fUux5s9HjC.jpg',
            title: 'John Doe',
            subtitle: 'johndoe@gmail.com',
          );
        },
      ),
    );
  }
}
