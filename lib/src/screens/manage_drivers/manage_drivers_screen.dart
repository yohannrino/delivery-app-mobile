import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../widgets/bottom_navigation_widget/bottom_navigation_widget_bloc.dart';
import '../../widgets/bottom_navigation_widget/bottom_navigation_widget_state.dart';
import '../base_screen.dart';

import 'banned_driver_page.dart';
import 'favorite_driver_page.dart';

class ManageDriversScreen extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() => _ManageDriversScreenState();
}

class _ManageDriversScreenState extends BaseState<ManageDriversScreen> {
  final PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: <BlocListener<Bloc<dynamic, dynamic>, dynamic>>[
        BlocListener<BottomNavigationWidgetBloc, BottomNavigationWidgetState>(
          listener: (BuildContext context, BottomNavigationWidgetState state) {
            if (state is IndexUpdated) {
              _pageController.animateToPage(
                state.selectedIndex,
                duration: const Duration(milliseconds: 400),
                curve: Curves.easeInOut,
              );
            }
          },
        ),
      ],
      child: Container(
        child: PageView(
          controller: _pageController,
          physics: const NeverScrollableScrollPhysics(),
          children: <Widget>[
            FavoriteDriverPage(),
            BannedDriverPage(),
          ],
        ),
      ),
    );
  }
}
