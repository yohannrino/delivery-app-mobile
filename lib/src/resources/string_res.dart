class StringRes {
  static const String appName = 'Delivery App';
  static const String emailAddress = 'Email Address';
  static const String password = 'Password';
  static const String signIn = 'Sign In';
  static const String placeOrder = 'Place Order';
  static const String orders = 'Orders';
  static const String wallet = 'Wallet';
  static const String manageDrivers = 'Manage Drivers';
  static const String settings = 'Settings';
  static const String logout = 'Logout';
  static const String itemDescription = 'Item Description';
  static const String pickUpLocation = 'Pick up Location';
  static const String dropOffLocation = 'Drop off location';
  static const String searchLocation = 'Search Location';
  static const String info = 'Info';
  static const String optional = 'Optional';
  static const String room = 'Room';
  static const String floor = 'Floor';
  static const String building = 'Building';
  static const String block = 'Block';
  static const String contactName = 'Contact Name';
  static const String contactPhone = 'Contact Phone';
  static const String close = 'Close';
  static const String search = 'Search';
  static const String remarks = 'Remarks';
  static const String deliver = 'Deliver';
  static const String onGoing = 'On-going';
  static const String completed = 'Completed';
  static const String cancelled = 'Cancelled';
  static const String favorite = 'Favorite';
  static const String banned = 'Banned';

  static const String errLocationNotFound = 'Location not found. Please try again.';
}