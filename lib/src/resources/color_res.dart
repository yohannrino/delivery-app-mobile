import 'package:flutter/material.dart';

class ColorRes {
  static const Color primary = Colors.blue;
  static const Color backgroundColor = Color(0xFFF5F5F5);
  static const Color lightGrey = Color(0xFFCDCDCD);
}