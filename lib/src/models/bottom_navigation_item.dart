import 'package:flutter/material.dart';

class BottomNavigationItem {
  BottomNavigationItem({
    @required this.title,
    @required this.activeColor,
    @required this.iconData,
  });

  final String title;
  final Color activeColor;
  final IconData iconData;
}
