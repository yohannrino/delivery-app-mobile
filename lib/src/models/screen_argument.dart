import 'transaction.dart';

class ScreenArgument {}

class MapScreenArgument extends ScreenArgument {
  MapScreenArgument({this.transaction, this.flag});

  final Transaction transaction;
  final int flag;
}
