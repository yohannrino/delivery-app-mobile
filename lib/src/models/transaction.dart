class Transaction {
  Transaction({
    this.id,
    this.remarks,
    this.pickUpLocation,
    this.dropOffLocation,
  });

  Transaction.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    remarks = json['remarks'];
    pickUpLocation = json['pickUpLocation'] != null
        ? PickUpLocation.fromJson(json['pickUpLocation'])
        : null;
    dropOffLocation = json['dropOffLocation'] != null
        ? DropOffLocation.fromJson(json['dropOffLocation'])
        : null;
  }

  int id;
  String remarks;
  PickUpLocation pickUpLocation;
  DropOffLocation dropOffLocation;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    if (pickUpLocation != null) {
      data['pickUpLocation'] = pickUpLocation.toJson();
    }
    if (dropOffLocation != null) {
      data['dropOffLocation'] = dropOffLocation.toJson();
    }
    return data;
  }
}

abstract class Address {
  Address({
    this.address,
    this.lat,
    this.lng,
    this.room,
    this.floor,
    this.building,
    this.block,
    this.contactName,
    this.contactPhone,
  });

  String address;
  double lat;
  double lng;
  String room;
  String floor;
  String building;
  String block;
  String contactName;
  String contactPhone;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['address'] = address;
    data['lat'] = lat;
    data['lng'] = lng;
    data['room'] = room;
    data['floor'] = floor;
    data['building'] = building;
    data['block'] = block;
    data['contactName'] = contactName;
    data['contactPhone'] = contactPhone;
    return data;
  }
}

class PickUpLocation extends Address {
  PickUpLocation();

  PickUpLocation.fromJson(Map<String, dynamic> json) {
    address = json['address'];
    lat = json['lat'];
    lng = json['lng'];
    room = json['room'];
    floor = json['floor'];
    building = json['building'];
    block = json['block'];
    contactName = json['contactName'];
    contactPhone = json['contactPhone'];
  }
}

class DropOffLocation extends Address {
  DropOffLocation();

  DropOffLocation.fromJson(Map<String, dynamic> json) {
    address = json['address'];
    lat = json['lat'];
    lng = json['lng'];
    room = json['room'];
    floor = json['floor'];
    building = json['building'];
    block = json['block'];
    contactName = json['contactName'];
    contactPhone = json['contactPhone'];
  }
}
