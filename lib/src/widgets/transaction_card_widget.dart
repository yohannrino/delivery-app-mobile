import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

import '../constants/Fonts.dart';

class TransactionCardWidget extends StatelessWidget {
  const TransactionCardWidget({@required this.borderColor});

  final Color borderColor;

  @override
  Widget build(BuildContext context) {
    return FlipCard(
      front: _buildFrontWidget(),
      back: _buildBackWidget(),
    );
  }

  Widget _buildContainer({@required Widget child}) {
    return Container(
      decoration: BoxDecoration(
        color: borderColor,
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.only(top: 24),
      child: Container(
        margin: const EdgeInsets.only(left: 24),
        padding: const EdgeInsets.all(16),
        color: Colors.white,
        child: child,
      ),
    );
  }

  Widget _buildFrontWidget() {
    return _buildContainer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                '# 12345678',
                style: TextStyle(
                  fontFamily: Fonts.avenirLTStdHeavy,
                  fontSize: 16,
                ),
              ),
              Text(
                'Dec, 29, 2018',
                style: TextStyle(
                  color: Colors.grey,
                  fontFamily: Fonts.avenirLTStdHeavy,
                  fontSize: 14,
                ),
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 16),
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 16),
                  child: Icon(Feather.circle),
                ),
                Expanded(
                  child: Text(
                    'Elliptical Rd, Diliman, Quezon City, Metro Manila',
                    style: TextStyle(fontFamily: Fonts.avenirLTStdMedium),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 24),
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 16),
                  child: Icon(Feather.map_pin),
                ),
                Expanded(
                  child: Text(
                    'Elliptical Rd, Diliman, Quezon City, Metro Manila',
                    style: TextStyle(fontFamily: Fonts.avenirLTStdMedium),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 4),
                  child: Text(
                    'PHP',
                    style: TextStyle(fontFamily: Fonts.avenirLTStdMedium),
                  ),
                ),
                Text(
                  '150',
                  style: TextStyle(
                    fontFamily: Fonts.avenirLTStdHeavy,
                    fontSize: 20,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBackWidget() {
    return _buildContainer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              SizedBox(
                height: 64,
                width: 64,
                child: CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://www.gstatic.com/webp/gallery/1.jpg'),
                ),
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'John Wick',
                        style: TextStyle(
                          fontFamily: Fonts.avenirLTStdHeavy,
                          fontSize: 18,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: Text(
                          'johnwick@gmail.com',
                          style: TextStyle(
                            color: Colors.grey,
                            fontFamily: Fonts.avenirLTStdMedium,
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                IconButton(
                  icon: const Icon(
                    Feather.phone_call,
                    size: 22,
                  ),
                  onPressed: () {},
                ),
                IconButton(
                  icon: const Icon(
                    Feather.message_circle,
                    size: 22,
                  ),
                  onPressed: () {},
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
