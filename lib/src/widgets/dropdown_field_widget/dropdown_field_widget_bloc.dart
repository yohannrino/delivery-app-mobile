import 'dart:async';
import 'package:bloc/bloc.dart';

import 'dropdown_field_widget_event.dart';
import 'dropdown_field_widget_state.dart';

class DropdownFieldWidgetBloc extends Bloc<DropdownFieldWidgetEvent, DropdownFieldWidgetState> {
  @override
  DropdownFieldWidgetState get initialState => InitialDropdownFieldWidgetState();

  @override
  Stream<DropdownFieldWidgetState> mapEventToState(
    DropdownFieldWidgetEvent event,
  ) async* {
    if (event is DropdownSelectItem) {
      yield DropdownValueChanged(event.value);
    }
  }
}
