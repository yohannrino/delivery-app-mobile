import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DropdownFieldWidgetEvent extends Equatable {
  DropdownFieldWidgetEvent([List<dynamic> props = const <dynamic>[]])
      : super(props);
}

class DropdownSelectItem extends DropdownFieldWidgetEvent {
  DropdownSelectItem(this.value) : super(<dynamic>[value]);

  final String value;
}
