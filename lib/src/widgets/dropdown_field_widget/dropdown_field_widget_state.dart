import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DropdownFieldWidgetState extends Equatable {
  DropdownFieldWidgetState([List<dynamic> props = const <dynamic>[]])
      : super(props);
}

class InitialDropdownFieldWidgetState extends DropdownFieldWidgetState {}

class DropdownValueChanged extends DropdownFieldWidgetState {
  DropdownValueChanged(this.value) : super(<dynamic>[value]);

  final String value;
}
