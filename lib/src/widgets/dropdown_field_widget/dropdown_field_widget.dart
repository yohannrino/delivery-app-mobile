import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants/Fonts.dart';
import 'dropdown_field_widget_bloc.dart';
import 'dropdown_field_widget_event.dart';
import 'dropdown_field_widget_state.dart';

class DropdownFieldWidget extends StatefulWidget {
  const DropdownFieldWidget({@required this.items, this.onChange});

  final List<String> items;
  final void Function(String) onChange;

  @override
  State<StatefulWidget> createState() => _DropdownFieldWidget();
}

class _DropdownFieldWidget extends State<DropdownFieldWidget> {
  final DropdownFieldWidgetBloc _dropdownFieldWidgetBloc =
      DropdownFieldWidgetBloc();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DropdownFieldWidgetBloc, DropdownFieldWidgetState>(
      bloc: _dropdownFieldWidgetBloc,
      builder: (BuildContext context, DropdownFieldWidgetState state) {
        return DropdownButtonFormField<String>(
          decoration: InputDecoration(
            labelText: 'Vehicle',
            labelStyle: const TextStyle(fontFamily: Fonts.avenirLTStdHeavy),
          ),
          value: state is DropdownValueChanged ? state.value : widget.items[0],
          onChanged: (String value) => _onChange(value),
          items: widget.items.map((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(
                value,
                style: const TextStyle(
                  fontFamily: Fonts.avenirLTStdHeavy,
                ),
              ),
            );
          }).toList(),
        );
      },
    );
  }

  @override
  void dispose() {
    _dropdownFieldWidgetBloc.dispose();
    super.dispose();
  }

  void _onChange(String value) {
    widget.onChange(value);
    _dropdownFieldWidgetBloc.dispatch(DropdownSelectItem(value));
  }
}
