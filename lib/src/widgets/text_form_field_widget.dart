import 'package:flutter/material.dart';

import '../constants/Fonts.dart';

class TextFormFieldWidget extends StatelessWidget {
  const TextFormFieldWidget({
    this.autoValidate,
    this.label,
    this.textInputAction,
    this.focusNode,
    this.controller,
    this.obscureText = false,
    this.textInputType,
    this.enabled,
    this.validator,
    this.onFieldSubmitted,
    this.suffixIcon,
    this.maxLines = 1,
    this.dark = false,
    this.labelStyle,
    this.textStyle,
    this.prefixIcon,
    this.initialValue,
    this.hasBorder = true,
    this.hintText,
    this.contentPadding,
    this.minLines,
  });

  final int maxLines;
  final int minLines;
  final bool dark;
  final bool enabled;
  final bool autoValidate;
  final bool obscureText;
  final bool hasBorder;
  final String label;
  final String initialValue;
  final String hintText;
  final TextInputAction textInputAction;
  final FocusNode focusNode;
  final TextEditingController controller;
  final TextInputType textInputType;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  final Widget suffixIcon;
  final TextStyle labelStyle;
  final TextStyle textStyle;
  final Widget prefixIcon;
  final EdgeInsets contentPadding;

  @override
  Widget build(BuildContext context) {
    final Color primary = dark ? Colors.white : Colors.black;
    final Color borderColor = dark ? Colors.white : Colors.grey;
    final Color labelColor = dark ? Colors.white70 : Colors.black54;
    final InputBorder inputBorder = hasBorder
        ? UnderlineInputBorder(
            borderSide: BorderSide(color: borderColor),
          )
        : InputBorder.none;

    return TextFormField(
      controller: controller,
      cursorColor: primary,
      decoration: InputDecoration(
        contentPadding: contentPadding,
        focusedBorder: inputBorder,
        enabledBorder: inputBorder,
        disabledBorder: inputBorder,
        labelText: label,
        labelStyle: TextStyle(
          color: labelColor,
          fontFamily: Fonts.avenirLTStdMedium,
        ).merge(labelStyle),
        hasFloatingPlaceholder: true,
        suffixIcon: suffixIcon,
        prefixIcon: prefixIcon,
        hintText: hintText,
        hintStyle: TextStyle(
          color: labelColor,
          fontFamily: Fonts.avenirLTStdMedium,
        ),
      ),
      enabled: enabled,
      focusNode: focusNode,
      obscureText: obscureText,
      textInputAction: textInputAction,
      keyboardType: textInputType,
      validator: validator,
      minLines: minLines,
      maxLines: maxLines,
      initialValue: initialValue,
      style: TextStyle(
        color: primary,
        fontSize: 18,
        fontFamily: Fonts.avenirLTStdMedium,
      ).merge(textStyle),
      onFieldSubmitted: onFieldSubmitted,
    );
  }
}
