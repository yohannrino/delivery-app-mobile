import 'package:flutter/material.dart';

import '../constants/Fonts.dart';
import '../resources/color_res.dart';

class DriverCardWidget extends StatelessWidget {
  const DriverCardWidget({
    this.url,
    this.title,
    this.subtitle,
  });

  final String url;
  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: ColorRes.lightGrey,
            blurRadius: 8,
          ),
        ],
      ),
      margin: const EdgeInsets.only(top: 16),
      child: ListTile(
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 12,
          vertical: 12,
        ),
        leading: CircleAvatar(
          backgroundImage: NetworkImage(url),
        ),
        title: Text(
          title,
          style: TextStyle(
            fontFamily: Fonts.avenirLTStdHeavy,
            fontSize: 16,
          ),
        ),
        subtitle: Text(
          subtitle,
          style: TextStyle(
            color: Colors.grey,
            fontFamily: Fonts.avenirLTStdMedium,
          ),
        ),
        trailing: Icon(
          Icons.remove_circle_outline,
          color: Colors.red,
        ),
        onTap: () {},
      ),
    );
  }
}
