import 'dart:async';
import 'package:bloc/bloc.dart';

import 'bottom_navigation_widget_event.dart';
import 'bottom_navigation_widget_state.dart';

class BottomNavigationWidgetBloc extends Bloc<BottomNavigationWidgetEvent, BottomNavigationWidgetState> {
  @override
  BottomNavigationWidgetState get initialState => InitialOrderBottomNavigationState();

  @override
  Stream<BottomNavigationWidgetState> mapEventToState(
    BottomNavigationWidgetEvent event,
  ) async* {
    if (event is UpdateIndex) {
      yield IndexUpdated(selectedIndex: event.selectedIndex);
    }
  }
}
