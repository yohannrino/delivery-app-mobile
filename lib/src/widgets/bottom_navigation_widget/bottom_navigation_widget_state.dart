import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BottomNavigationWidgetState extends Equatable {
  BottomNavigationWidgetState([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class InitialOrderBottomNavigationState extends BottomNavigationWidgetState {}

class IndexUpdated extends BottomNavigationWidgetState {
  IndexUpdated({this.selectedIndex}) : super(<dynamic>[selectedIndex]);

  final int selectedIndex;
}
