import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants/Fonts.dart';
import '../../models/bottom_navigation_item.dart';

import 'bottom_navigation_widget_bloc.dart';
import 'bottom_navigation_widget_event.dart';
import 'bottom_navigation_widget_state.dart';

class BottomNavigationWidget extends StatefulWidget {
  const BottomNavigationWidget({@required this.menuList});

  final List<BottomNavigationItem> menuList;

  @override
  State<StatefulWidget> createState() => _BottomNavigationWidgetState();
}

class _BottomNavigationWidgetState extends State<BottomNavigationWidget> {
  BottomNavigationWidgetBloc _orderBottomNavigationBloc;

  @override
  void initState() {
    _orderBottomNavigationBloc =
        BlocProvider.of<BottomNavigationWidgetBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BottomNavigationWidgetBloc, BottomNavigationWidgetState>(
      bloc: _orderBottomNavigationBloc,
      builder: (BuildContext context, BottomNavigationWidgetState state) {
        int currentIndex = 0;

        if (state is IndexUpdated) {
          currentIndex = state.selectedIndex;
        }

        return BottomNavyBar(
          selectedIndex: currentIndex,
          items: _buildBottomNavBarItems(),
          onItemSelected: _onItemSelected,
        );
      },
    );
  }

  @override
  void dispose() {
    _orderBottomNavigationBloc.dispose();
    super.dispose();
  }

  List<BottomNavyBarItem> _buildBottomNavBarItems() {
    final List<BottomNavyBarItem> list = <BottomNavyBarItem>[];

    widget.menuList.map((BottomNavigationItem item) {
      list.add(BottomNavyBarItem(
        icon: Icon(item.iconData),
        title: Text(
          item.title,
          style: const TextStyle(
            fontFamily: Fonts.avenirLTStdHeavy,
          ),
        ),
        activeColor: item.activeColor,
        inactiveColor: Colors.black,
      ));
    }).toList();

    return list;
  }

  void _onItemSelected(int index) {
    _orderBottomNavigationBloc.dispatch(UpdateIndex(selectedIndex: index));
  }
}
