import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BottomNavigationWidgetEvent extends Equatable {
  BottomNavigationWidgetEvent([List<dynamic> props = const <dynamic>[]]) : super(props);
}

class UpdateIndex extends BottomNavigationWidgetEvent {
  UpdateIndex({this.selectedIndex}) : super(<dynamic>[selectedIndex]);

  final int selectedIndex;
}
