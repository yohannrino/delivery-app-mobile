import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:hidden_drawer_menu/hidden_drawer/hidden_drawer_menu.dart';

import '../constants/Fonts.dart';
import '../resources/string_res.dart';

class MainDrawerMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainDrawerMenuState();
}

class _MainDrawerMenuState extends State {
  final List<Map<String, dynamic>> menuList = <Map<String, dynamic>>[
    <String, dynamic>{'text': StringRes.placeOrder, 'icon': Feather.truck},
    <String, dynamic>{'text': StringRes.orders, 'icon': Feather.package},
    <String, dynamic>{'text': StringRes.manageDrivers, 'icon': Feather.users},
    <String, dynamic>{'text': StringRes.wallet, 'icon': Feather.credit_card},
    <String, dynamic>{'text': StringRes.settings, 'icon': Feather.settings},
    <String, dynamic>{'text': StringRes.logout, 'icon': Feather.log_out}
  ];

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.blueAccent,
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buildListWidget(),
        ),
      ),
    );
  }

  Widget _buildProfileContainer() {
    return Container(
        margin: const EdgeInsets.only(bottom: 40),
        padding: const EdgeInsets.only(left: 32, right: 32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CircleAvatar(
              radius: 40,
              backgroundImage:
                  NetworkImage('https://www.gstatic.com/webp/gallery/1.jpg'),
            ),
            Container(
              margin: const EdgeInsets.only(top: 8.0),
              child: const Text(
                'John Wick',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: Fonts.avenirLTStdHeavy,
                  fontSize: 16,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 4.0),
              child: const Text(
                'johnwick@gmail.com',
                style: TextStyle(
                  color: Colors.white70,
                  fontFamily: Fonts.avenirLTStdBook,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ));
  }

  List<Widget> _buildListWidget() {
    final List<Widget> widgetList = <Widget>[];

    widgetList.add(_buildProfileContainer());
    menuList.map((Map<String, dynamic> value) {
      final int index = menuList.indexOf(value);
      widgetList.add(
        FlatButton(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Icon(
                  value['icon'],
                  color: Colors.white,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  value['text'],
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: Fonts.avenirLTStdMedium,
                    fontSize: 18,
                  ),
                ),
              ),
            ],
          ),
          onPressed: () => _onMenuListItemPressed(index),
        ),
      );
    }).toList();

    return widgetList;
  }

  void _onMenuListItemPressed(int index) {
    SimpleHiddenDrawerProvider.of(context).setSelectedMenuPosition(index);
  }
}
