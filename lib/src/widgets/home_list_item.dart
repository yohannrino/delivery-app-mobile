import 'package:flutter/material.dart';

import '../resources/color_res.dart';

class HomeListItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: ColorRes.lightGrey,
              blurRadius: 15.0,
            ),
          ],
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
        ),
      ),
    );
  }
}

