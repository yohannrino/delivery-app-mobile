import 'package:flutter/material.dart';

import 'package:deliveryapp/src/constants/Fonts.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  const AppBarWidget({
    @required this.title,
    this.leading,
    this.hasBorder = true,
    this.containerHeight = 144,
    this.actions,
  });

  final String title;
  final Widget leading;
  final List<Widget> actions;
  final bool hasBorder;
  final double containerHeight;

  @override
  Widget build(BuildContext context) {
    final MediaQueryData mediaQuery = MediaQuery.of(context);

    return Container(
      height: containerHeight,
      padding: const EdgeInsets.all(6),
      decoration: BoxDecoration(color: Colors.blue),
      child: Container(
        margin: EdgeInsets.only(top: mediaQuery.padding.top),
        child: Column(
          children: <Widget>[
            _buildTopWidget(),
            Container(
              margin: const EdgeInsets.only(top: 12, left: 12),
              child: Row(
                children: <Widget>[
                  Text(
                    title,
                    style: const TextStyle(
                      color: Colors.white,
                      fontFamily: Fonts.avenirLTStdHeavy,
                      fontSize: 28,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size(0, containerHeight);

  Widget _buildTopWidget() {
    if (leading == null && actions.isEmpty) {
      return Container();
    }

    return Row(
      children: <Widget>[
        leading,
        actions != null
            ? Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: actions,
                ),
              )
            : Container(),
      ],
    );
  }
}
