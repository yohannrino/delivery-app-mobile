class Fonts {
  static const String avenirLTStdBook = 'AvenirLTStdBook';
  static const String avenirLTStdHeavy = 'AvenirLTStdHeavy';
  static const String avenirLTStdLight = 'AvenirLTStdLight';
  static const String avenirLTStdMedium = 'AvenirLTStdMedium';
  static const String avenirLTStdRoman = 'AvenirLTStdRoman';
}
