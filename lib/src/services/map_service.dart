import 'package:dio/dio.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../models/geocode.dart';

import 'base_service.dart';

class MapService extends BaseService {
  final String _apiKey = 'AIzaSyC1wPI5Llbe_qbb8cm2yWGMLTfQnSRJ7rg';
  final String _api = 'https://maps.googleapis.com/maps/api';

  Future<Geocode> getGeocode({String address, LatLng latLng}) async {
    return Geocode.fromJson(await get(
      '/geocode/json',
      <String, String>{
        'address': address,
        'latlng': latLng == null ? '' : '${latLng.latitude},${latLng.longitude}',
        'key': _apiKey,
      },
      BaseOptions(baseUrl: _api),
    ));
  }
}
