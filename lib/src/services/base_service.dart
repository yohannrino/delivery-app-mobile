import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

import '../../dio_client.dart';
import '../../locator.dart';
import '../managers/cancel_token_manager.dart';

abstract class BaseService {
  @protected
  final DioClient _dioClient = sl.get<DioClient>();

  final Logger _logger = Logger();

  @protected
  Future<Map<String, dynamic>> post(
    String path,
    Map<String, dynamic> data,
  ) async {
    final CancelToken cancelToken = CancelToken();

    CancelTokenManager.addToken(cancelToken);

    try {
      final Response<String> response = await _dioClient.dio.post<String>(
        path,
        data: data,
        cancelToken: cancelToken,
      );
      final Map<String, dynamic> mapData = jsonDecode(response.data);
      _logger.d(mapData);

      // Remove in production
      _logger.d(<String, dynamic>{
        'path': '${response.request.baseUrl}${response.request.path}',
        'params': response.request.data,
        'statusCode': response.statusCode,
        'statusMessage': response.statusMessage,
      });

      return mapData;
    } on DioError catch (e) {
      _logger.e(e);
      return <String, dynamic>{};
    }
  }

  @protected
  Future<Map<String, dynamic>> get(String path, Map<String, dynamic> data,
      [BaseOptions baseOptions]) async {
    final CancelToken cancelToken = CancelToken();

    CancelTokenManager.addToken(cancelToken);

    _dioClient.setBaseOptions(baseOptions);

    try {
      final Response<String> response = await _dioClient.dio.get<String>(
        path,
        queryParameters: data,
        cancelToken: cancelToken,
      );
      final Map<String, dynamic> mapData = jsonDecode(response.data);
      _logger.d(mapData);

      // Remove in production
      _logger.i(<String, dynamic>{
        'path': '${response.request.baseUrl}${response.request.path}',
        'params': response.request.queryParameters,
        'statusCode': response.statusCode,
        'statusMessage': response.statusMessage,
      });

      return mapData;
    } on DioError catch (e) {
      _logger.e(e);
      return <String, dynamic>{};
    }
  }
}
