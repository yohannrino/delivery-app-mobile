enum Rule { required, alpha, email }

class ValidatorUtil {
  static const String _alphaRegex = '[a-zA-Z]';
  static const String _emailRegex =
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9]+\.[a-zA-Z]+";

  static String validate(String value, List<Rule> rules) {
    String error;

    for (Rule rule in rules) {
      switch (rule) {
        case Rule.required:
          if (value.isEmpty) {
            error = 'Please fill out this field.';
          }
          break;
        case Rule.alpha:
          if (!RegExp(_alphaRegex).hasMatch(value)) {
            error = 'Numeric and special characters are not allowed.';
          }
          break;
        case Rule.email:
          if (!RegExp(_emailRegex).hasMatch(value)) {
            error = 'Please enter a valid email address.';
          }
          break;
        default:
          error = null;
      }

      if (error != null) {
        break;
      }
    }
    return error;
  }
}
