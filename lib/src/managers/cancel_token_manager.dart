import 'package:dio/dio.dart';

class CancelTokenManager {
  CancelTokenManager._();

  static List<CancelToken> _tokenList = <CancelToken>[];

  static void addToken(CancelToken cancelToken) {
    _tokenList.add(cancelToken);
  }

  static void cancel() {
    _tokenList.forEach(_cancelRequest);
    _tokenList = <CancelToken>[];
  }

  static void _cancelRequest(CancelToken cancelToken) {
    cancelToken.cancel();
  }
}
