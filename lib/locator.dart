import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import 'dio_client.dart';
import 'navigator_service.dart';
import 'src/services/map_service.dart';

GetIt sl = GetIt.instance;

void setupLocator() {
  final DioClient dioClient = DioClient.createInstance(
    baseOptions: BaseOptions(),
  );

  sl.registerSingleton<DioClient>(dioClient);
  sl.registerSingleton<NavigatorService>(NavigatorService());

  sl.registerSingleton<MapService>(MapService());
}
