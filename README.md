# Delivery App
### Technologies Involved
- [flutter](https://flutter.dev/) - Google’s UI toolkit for building beautiful, natively compiled applications for mobile, web, and desktop from a single codebase.
- [flutter_bloc](https://felangel.github.io/bloc/#/flutterbloccoreconcepts) - A state managemnet for flutter.
- [dio](https://pub.dev/packages/dio) - A powerful Http client for Dart.
- [get_it](https://pub.dev/packages/get_it) - A Service Locator for Dart and Flutter projects.

### Installation

Delivery App requires **flutter**. Please visit this [link](https://flutter.dev/docs/get-started/install) on how install flutter

```sh
$ clone repo
$ flutter run
```

### Set up an editor for flutter
- **Android Studio / IntelliJ** - https://flutter.dev/docs/get-started/editor?tab=androidstudio
- **VSCode** - https://flutter.dev/docs/get-started/editor?tab=vscode

### Extension 
- **VSCode**
    - DevTools - https://flutter.dev/docs/development/tools/devtools/vscode
    *Note:* Running `flutter run` from the terminal won't open the DevTools extension. Instead press `F5` or visit this [link](https://flutter.dev/docs/development/tools/devtools/cli) how to run DevTools from the terminal
    - bloc - https://marketplace.visualstudio.com/items?itemName=FelixAngelov.bloc
